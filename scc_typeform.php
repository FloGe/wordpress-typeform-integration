<?php

/**
 * Plugin Name:       Type form integration with sumission limitatiom
 * Description:       Embed typeforms in wordpress an allow limitting number of submissions for the the form for unique ids. This solved with hidden fields in the typeform
 * Version:           0.1
 * Author:            Smart Cities Consulting
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

if (!defined('ABSPATH')) {
    exit;
}




 function scc_typeform_init()
 {
     // WP Globals
     global $wpdb;

     // Table for unique identifiers
     $sccUniqueTable = $wpdb->prefix . '_scctypeform_uniques';
     $charset_collate = $wpdb->get_charset_collate();

     // Create Table if not exist

     // Include Upgrade Script
     require_once(ABSPATH . '/wp-admin/includes/upgrade.php');

     if ($wpdb->get_var("show tables like '$sccUniqueTable'") != $sccUniqueTable) {

        // Query - Create Table
         $sql = "CREATE TABLE `$sccUniqueTable` (";
         $sql .= " `formid` varchar(20) NOT NULL, ";
         $sql .= " `token` varchar(56) NOT NULL, ";
         $sql .= " `count` int, ";
         $sql .= "PRIMARY KEY (formid, token)";
         $sql .= ") $charset_collate;";
    
         // Create Table
         dbDelta($sql);
     }
 }

 function scc_typeform_deactivate()
 {
     // WP Globals
     global $wpdb;

     // Table
     $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';
     // delete table
     $wpdb->query("DROP TABLE IF EXISTS $sccUniqueTable");
 }

 
/**
 * AJAX endpoint to update counter for from and token
 */
 function scc_typeform_submitted()
 {
     // WP Globals
     global $wpdb;

     if (! wp_verify_nonce($_POST['nonce'], 'scctypeform-nonce')) {
         die();
     }

     $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';

     $formid = sanitize_title($_POST["formid"]);
     $token = sanitize_key($_POST["token"]);
     $private = sanitize_key($_POST["private"]);

     if ($private !="1" && scc_typeform_check($formid, $token) == "empty") {
         // If the form is not set to private and there is no counter in database for form and token insert them
         $sql = $wpdb->prepare("INSERT INTO $sccUniqueTable (formid, token, count) VALUES (%s,%s,1);", $formid, $token);
     } else {
         // If there is already an entry in the db for form and token update entry
         // Since a entry in db is created for private forms with counter set to 0
         // we can update the entry for private forms and non private Forms with existing db entry
         // If the token is not valid no entry will be created for private forms
         $sql = $wpdb->prepare("UPDATE $sccUniqueTable SET count = count + 1 WHERE formid = %s AND  token = %s ", $formid, $token);
     }

     $wpdb->query($sql);

     wp_send_json("updated", 200);
 }

 /**
  * Function to get the number of submissions for form and token
  */
 function scc_typeform_check($formid, $token)
 {
     global $wpdb;

     $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';

     // Sum count field for form and token to tackle multiple lines in DB.
     $sql = $wpdb->prepare("SELECT SUM(count) as count FROM $sccUniqueTable WHERE formid = %s AND  token = %s;", $formid, $token);
     
     $result = $wpdb->get_results($sql);

     $count = $result[0]->count;

     // set the count to empty if null so we can better distinquies between results where the counter is 0 and where no entry in db.
     if ($count == null) {
         $count = "empty";
     }
     return $count;
 }

/**
 * AJAX endpoint to the number of submissions for form and token
 */
function scc_typeform_check_send()
{
    if (! wp_verify_nonce($_POST['nonce'], 'scctypeform-nonce')) {
        die();
    }

    $formid = sanitize_title($_POST["formid"]);
    $token = sanitize_key($_POST["token"]);
    
    $data = scc_typeform_check($formid, $token);
  
    wp_send_json($data, 200);
}

/**
 * Get the unique tokens for a form as a string
 *
 * @param string $formid
 *   The form id
 *
 * @return string
 *   The unique tokens concatenated, delimited by a comma
 */
function scc_typeform_get_uniques($formid)
{
    global $wpdb;

    $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';

    $sql = $wpdb->prepare("SELECT token  FROM $sccUniqueTable WHERE formid = %s", $formid);

    $result = $wpdb->get_results($sql);

    $uniques ="";
    foreach ($result as $row) {
        $uniques = $uniques . $row->token . ",";
    }

    return trim($uniques, ",");
}

/**
 * Delete all entries for a form from the database
 * @param string $formid
 *   The form id
 */
function scc_typeform_remove_form($formid)
{
    global $wpdb;

    $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';
    $sccExpiredTable = $wpdb->prefix . '_scctypeform_expired_text';

    $sql = $wpdb->prepare("DELETE  FROM $sccUniqueTable WHERE formid = %s", $formid);

    $result = $wpdb->query($sql);

    return;
}


/**
 * Save the tokens for a form in db
 *
 * @param string $formid
 *   The formid
 * @param string $values
 *   The tokens to store separated by comma
 */
function scc_typeform_write_uniques($formid, $values)
{
    global $wpdb;

    $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';
    // get already saved values as array
    $old = explode(",", scc_typeform_get_uniques($formid));
    // get new values as array
    $new = explode(",", trim($values));

    // Get all tokens only present in the db
    $deletes = array_diff($old, $new);

    // sql template for deleting
    $delete_sql = "DELETE from $sccUniqueTable WHERE formid = %s AND  token = %s ";
    
    // get all tokens only present in the new list of tokens
    $updates = array_diff($new, $old);
    //sql template for inserting
    $insert_sql = "INSERT INTO $sccUniqueTable (formid, token, count) VALUES (%s,%s,0);";
  
    // delete tokens not in list any more
    foreach ($deletes as $delete) {
        $sql = $wpdb->prepare($delete_sql, $formid, $delete);
        $wpdb->query($sql);
    }

    // insert new tokens
    foreach ($updates as $insert) {
        $sql = $wpdb->prepare($insert_sql, $formid, sanitize_key($insert));
        $wpdb->query($sql);
    }
}

/**
 * get all form ids stored in the dab
 *
 * @return string[]
 *   Array of form ids
 */
function scc_typeform_get_forms()
{
    global $wpdb;

    $forms = array();
    $sccUniqueTable =$wpdb->prefix . 'scctypeform_uniques';
    $result = $wpdb->get_results("SELECT DISTINCT formid  FROM $sccUniqueTable");
    
    if ($result) {
        foreach ($result as $id) {
            $forms[] = $id->formid;
        }
    }
  
    return $forms;
}

/**
 * Callback to write the HTML of the options page
 */
function scc_typeform_admin_page_html()
{
    // build array of forms and their settings
    $forms = scc_typeform_get_forms();
    $settings = array();
    $row= 0;

    foreach ($forms as $form) {
        $settings[$form]["token"] = scc_typeform_get_uniques($form);
    }

    echo '<div class="wrap">';
    echo "<h1>" . esc_html(get_admin_page_title()) . "</h1>";
    echo '<form action="' . admin_url("admin-post.php") . '" method="post">';
    echo '<input type="hidden" name="action" value="scc_typeform_setup">';
    echo '<table>';
    echo '<tr>';
    echo '<th>Form</th><th>valid tokens</th><th>operation</th>';
    echo '</tr>';
    foreach ($settings as $id => $setting) {
        echo '<tr><td><input type="text" name="scctypeform[' . $row . '][id]" value="' .$id . '"></td><td><textarea name="scctypeform[' . $row . '][tokens]" cols="50" rows="3">' . $setting["token"] . '</textarea></td> <td><input type="submit" name="scctypeform[' . $row . '][remove]" value="Remove"></td></tr>';
        $row = $row + 1;
    }
    echo '<tr><td><input type="text" name="scctypeform[' . $row . '][id]" value=""></td><td><textarea name="scctypeform[' . $row . '][tokens]" cols="50" rows="3"></textarea></td></tr>';
    echo '</tr>';
    echo '</table>';
    echo '<p><b>Usage</b></p>';
    echo '<p>to embed a TypeForm enter the shortcode [scctypeform form="id of typeform" maxcount="max number of submissions" token-identifier="query paramerter with unique id" private="1 if acces only with unique id"]text if no acces to form[/scctypeform]</p>';

    echo '<input type="submit" value="Submit">';
    echo "</form>";
    echo "</div>";
}

/**
 * Create teh options page
 */
function scc_typeform_admin_page()
{
    add_menu_page(
        'TypeForm integration options',
        'Typeform',
        'edit_posts',
        'scc_typeform',
        'scc_typeform_admin_page_html',
    );
}

/**
 * Save options
 */
function scc_typeform_save()
{
    $settings = $_POST['scctypeform'];
    $location = admin_url("admin.php?page=scc_typeform");
    foreach ($settings as $form) {
        if (isset($form["remove"])) {
            // If forms remove button was clicked remove Form
            scc_typeform_remove_form($form["id"]);
        } else {
            // Save Form
            scc_typeform_write_uniques($form["id"], $form["tokens"]);
        }
    }
    // reload options page
    wp_redirect($location, 303);
    exit;
}

/**
 * Replace shortcode with HTML
 */
function scc_typeform_shortcode($atts = [], $content, $tag)
{
    $params = shortcode_atts(array(
    'form' => '',
    'maxcount' => 0,
    'token-identifier' => "token",
    'private' => 0,
   ), $atts);

    $html = '<div class="scc-typeform" data-form="' . $params["form"] . '" data-maxcount="' . $params["maxcount"] . '" data-token="' . $params["token-identifier"] . '" data-private="' . $params["private"] . '">' . $content . '</div>';

    return $html;
}

/**
 * Load js and css for embedding Typeform
 */
function scc_typeform_assets()
{
    global $post;
    // we onle ned to load if there is a shortcode for embeding Typeform
    if (has_shortcode($post->post_content, "scctypeform")) {
        wp_register_script("scc_typeform", "//embed.typeform.com/next/embed.js");
        wp_register_style("scc_typeform_style", "//embed.typeform.com/next/css/widget.css");
        wp_register_script("scc_typeform_embed", plugin_dir_url(__FILE__) . "/lib/scc-typeform.js", array("jquery","scc_typeform"), null, true);

        wp_enqueue_script("scc_typeform");
        wp_enqueue_script("scc_typeform_embed");
        wp_enqueue_style("scc_typeform_style");

        // inject url and nonce to page
        wp_localize_script("scc_typeform_embed", "scctypeform_ajax", array("url" =>admin_url('admin-ajax.php'),'nonce' => wp_create_nonce('scctypeform-nonce'),));
    }
}

// when plugin is activated
register_activation_hook(__FILE__, "scc_typeform_init");
// when plugin is de-activation
register_deactivation_hook(__FILE__, "scc_typeform_deactivate");

// add options page
add_action('admin_menu', 'scc_typeform_admin_page');

// register callback for custom option handling
add_action("admin_post_scc_typeform_setup", "scc_typeform_save");

// we use a shortcode
add_shortcode('scctypeform', 'scc_typeform_shortcode');



//setup ajaxendpoints

add_action('wp_ajax_scc_typeform_submit', 'scc_typeform_submitted');
add_action('wp_ajax_nopriv_scc_typeform_submit', 'scc_typeform_submitted');

add_action('wp_ajax_scc_typeform_check', 'scc_typeform_check_send');
add_action('wp_ajax_nopriv_scc_typeform_check', 'scc_typeform_check_send');

// js we need

add_action('wp_enqueue_scripts', 'scc_typeform_assets');
