## Features
This plugin helps to embed surveys made with [Typeform](https://www.typeform.com) into your wordpress site with two additional feature:
* embed the form only when the sites visitor has a valid token.
* embed the form only when the sites visitor has a token and the token was not used to submit the survey for a defined number of times.

## usage

### Type form
* Create your survey on [Typeform](https://www.typeform.com) and add a hidden field (e.g. id)
* Setup a list of identifiers and prepare a link list to the embeded form in the format
https://your.wordpress.com/pagewith/typeform?id=xxxxxx to send to the partizipants of your survey.

### Wordpress

* Go to /wp-admin/admin.php?page=scc_typeform of your worpress installation and enter form id an the list of tokens separated by coma into the settings.
* embed the Form with following shorcode into your article:
    [scctypeform form="id of typeform" maxcount="max number of submissions" token-identifier="query paramerter with unique id" private="1 or 0"]text if no acces to form[/scctypeform]
The parameters of the short code are:
* form : the TypeForm id of the form
* maxcount how many often can the form be submitted with an specific token?
* token-identifier: the query parameter in the url of the page (the hidden field name in the typeform)
* private: is the form only accesible with an valid token (entered in the configuration page before)

The form will only be embedded if 
  * there is a valid token in the url and private is set to 1
  * There is a (valid) token in the url and the number of subissions with this token does not exceed maxcount

If maxcount is 0 submission is unlimited.
