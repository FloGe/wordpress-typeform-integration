(function($) {
  $(".scc-typeform").each(function(){

    // get varibale we need
    var formid = $(this).attr("data-form");
    var maxcount = $(this).attr("data-maxcount");
    var token_param = $(this).attr("data-token");
    var private = $(this).attr("data-private");
    var urlParams = new URLSearchParams(window.location.search);
    var token = "anonymous";
    var url = scctypeform_ajax.url;
    var that = this;
    
    if(urlParams.has(token_param)){
      token = urlParams.get(token_param);
    }

    // we are finished if token is anonymous and the form is private
    if(token == "anonymous" && private == 1){
      return;
    }
    
    // count submissions with the token
    $.ajax(scctypeform_ajax.url,{
      method: "POST",
      data:{
        action : "scc_typeform_check", 
        nonce: scctypeform_ajax.nonce,
        formid: formid,
        token: token,
      }
    }).done((data)=> {

      //Private tokens must have an entry in database
      if(data == "empty" && private == 1){
        return;
      }

      var submissions = 0
      if(data != "empty"){
        submissions = data;
      }
      if((parseInt(maxcount, 10) > 0 &&  parseInt(maxcount,10) > parseInt(submissions,10)) || parseInt(maxcount, 10) == 0){
        window.tf.createWidget(formid,{
          container: that,
          height: 600,
          transitiveSearchParams: [token_param],
          onSubmit: (data)=>{
            $.ajax(scctypeform_ajax.url,{
              method: "POST",
              data: {"formid":formid,"token":token,"private": private,"action":'scc_typeform_submit', "nonce": scctypeform_ajax.nonce }
            }).done(function(response){console.log(response);});
          }
        });
      }
    });





  });
})( jQuery );